﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameSceneScript : MonoBehaviour
{

	public GameObject balloonPrefab;
	public GameObject onePlusView;

	private int BALLOON_MAX_COUNT = 10;
	private int score;
	private Text scoreText;
	public static GameSceneScript instance = null;

	void Start()
	{

		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
		}


		scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
		score = 0;
		UpdateScore();
	}

	void Update()
	{

		// run balloons
		GameObject[] balloons = GameObject.FindGameObjectsWithTag("Balloon");
		if (balloons.Length < BALLOON_MAX_COUNT)
		{
			RunNewBalloon();
		}

		// click/touch
#if UNITY_STANDALONE || UNITY_EDITOR

		if (Input.GetMouseButtonDown(0))
		{
            PopBalloonIfHit(Input.mousePosition);
		}
#else

		foreach (Touch t in Input.touches)
		{
			if (t.phase == TouchPhase.Began)
			{
				PopBalloonIfHit(t.position);
			}
		}

#endif
	}

	void PopBalloonIfHit(Vector3 position)
	{
		Vector3 touchPosition = Camera.main.ScreenToWorldPoint(position);
		Vector2 touchPosition2D = new Vector2(touchPosition.x, touchPosition.y);
		RaycastHit2D hit = Physics2D.Raycast(touchPosition2D, Vector2.zero);
		if (hit.collider != null) {
			if (hit.collider.gameObject.tag == "Balloon")
			{
				BalloonScript balloon = hit.collider.gameObject.GetComponent<BalloonScript>() as BalloonScript;
				balloon.Pop();
			}
		}
	}


	void RunNewBalloon()
	{
		Instantiate(balloonPrefab, new Vector3(), Quaternion.identity);
	}

	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
	}

	public void IncrementScore(int value)
	{
		score += value;
		UpdateScore();
	}

	public void ShowOnHitEffect(int value, Vector3 position, Color color)
	{
		GameObject onePlus = Instantiate(onePlusView, position, Quaternion.identity);
		onePlus.GetComponent<TextMesh>().text = "+" + value.ToString();
		onePlus.GetComponent<TextMesh>().color = color;
	}
}
