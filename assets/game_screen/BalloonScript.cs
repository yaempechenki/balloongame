﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BalloonScript : MonoBehaviour
{


	private float SPEED = 9;

	private float SIZE_MIN = 0.8f;
	private float SIZE_MAX = 6f;

	void Start()
	{

		// scale
		transform.localScale = new Vector3(1f, 1f, 0) * Random.Range(SIZE_MIN, SIZE_MAX);

		// color
		Color newColor = new Color(Random.Range(0.15f, 1f), Random.Range(0.15f, 1f), Random.Range(0.15f, 1f), 1f);
		GetComponent<Renderer>().material.color = newColor;

		//position
		float radius = GetComponent<CircleCollider2D>().radius * transform.localScale.x;
		Vector2 screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

		float xPos = Random.Range(-screenBounds.x + radius, screenBounds.x - radius);
		transform.position = new Vector3(xPos, screenBounds.y + radius, 0);

		// end position
		Vector2 end = new Vector3(gameObject.transform.position.x, -gameObject.transform.position.y - radius, 0);
		StartCoroutine(SmoothMovement(end));

		SoundManager.instance.PlayOnBalloonStart();
	}


	IEnumerator SmoothMovement(Vector3 end)
	{

		float curSpeed = SPEED / transform.localScale.x;

		float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

		while (sqrRemainingDistance > float.Epsilon)
		{

			Vector3 newPosition = Vector3.MoveTowards(gameObject.transform.position, end, curSpeed * Time.deltaTime);
			gameObject.transform.position = newPosition;
			sqrRemainingDistance = (transform.position - end).sqrMagnitude;

			yield return null;
		}

		Destroy(gameObject);
	}

	public void Pop()
	{
		int score = (int)Mathf.Round(SIZE_MAX / transform.localScale.x);

		GameSceneScript.instance.IncrementScore(score);
		GameSceneScript.instance.ShowOnHitEffect(score, gameObject.transform.position, GetComponent<Renderer>().material.color);

		SoundManager.instance.PlayOnBalloonStop();


		Destroy(gameObject);
	}
}
