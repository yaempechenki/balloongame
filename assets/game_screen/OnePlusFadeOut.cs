﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnePlusFadeOut : MonoBehaviour {

	void Start () {
		transform.localScale = new Vector3 (0.3f, 0.3f, 1f);
		Vector2 end = gameObject.transform.position + new Vector3 (0, 1.2f, 0);
		StartCoroutine (SmoothMovement (end));
	}

	IEnumerator SmoothMovement(Vector3 end) {

		float inverseMoveTime = 1f/0.75f;

		float sqrRemainingDistance = (transform.position - end).sqrMagnitude; 

		while (sqrRemainingDistance > float.Epsilon) {
			Vector3 newPosition = Vector3.MoveTowards (gameObject.transform.position, end, inverseMoveTime * Time.deltaTime);
			gameObject.transform.position = newPosition;
			sqrRemainingDistance = (transform.position - end).sqrMagnitude;

			Color prevColor = GetComponent<Renderer> ().material.color;
			Color newColor = new Color (prevColor.r, prevColor.g, prevColor.b, prevColor.a - 0.02f);
			GetComponent<Renderer> ().material.color = newColor;

			yield return null;
		}

		Destroy (gameObject);
	}
}
