﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

	public AudioSource fxSource;
	public AudioClip onBallonStart;
	public AudioClip onBalloonStop;

	public static SoundManager instance = null;

	public float lowPitchRange = 0.95f;
	public float hightPitchRange = 1.05f;

	// Use this for initialization
	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
		}

		//DontDestroyOnLoad (gameObject);  
	}

	public void PlayOnBalloonStart()
	{
		RandomizeFX(onBallonStart);
	}

	public void PlayOnBalloonStop()
	{
		RandomizeFX(onBalloonStop);
	}

	public void RandomizeFX(AudioClip clip)
	{

		float randomPitch = Random.Range(lowPitchRange, hightPitchRange);

		fxSource.pitch = randomPitch;
		fxSource.clip = clip;
		fxSource.Play();
	}
}