﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{

	public string BundleName;

	public string SceneName;
	public string manifestName;
	private AssetBundleManifest manifest;


	IEnumerator Start()
	{
		string path = "file://" + Application.dataPath + "/../Data/";

		if (SystemInfo.deviceType == DeviceType.Desktop)
		{
			path += "OSX/Data/";
		}
		else if (SystemInfo.deviceType == DeviceType.Handheld)
		{
			path += "IOS/Data/";
		}
		else
		{
			path += "WIN/Data/";
		}

		Debug.Log("Loading assets from path " + path);
		// load manifest
		using (WWW www = new WWW(path + manifestName))
		{
			yield return www;

			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.Log(www.error);
				yield break;
			}


			manifest = www.assetBundle.LoadAsset("AssetBundleManifest") as AssetBundleManifest;
			yield return null;

			www.assetBundle.Unload(false);
		}


		//CleanCache ();


		// load asset bundle with manifest version
		using (WWW www = WWW.LoadFromCacheOrDownload(path + BundleName, manifest.GetAssetBundleHash(BundleName)))
		{


			yield return www;

			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.Log(www.error);
				yield break;
			}

			SceneManager.LoadScene(SceneName);
			yield return null;
			www.assetBundle.Unload(false);

		}
	}

	public static void CleanCache()
	{
		if (Caching.CleanCache())
		{
			Debug.Log("Successfully cleaned the cache.");
		}
		else
		{
			Debug.Log("Cache is being used.");
		}
	}
}