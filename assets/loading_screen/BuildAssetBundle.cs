﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;


public class BuildAssetBundle
{

	[MenuItem("Asset Bundles/Build OSX")]
	static void BuildBundles()
	{
		string path = EditorUtility.SaveFolderPanel("Save OSX bundles to...", "", ""); // dialog window
		if (path.Length != 0)
		{
			BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.None, BuildTarget.StandaloneOSXIntel);
		}
	}

	[MenuItem("Asset Bundles/Build iOS")]
	static void BuildBundlesIos()
	{
		string path = EditorUtility.SaveFolderPanel("Save IOS bundles to...", "", ""); // dialog window
		if (path.Length != 0)
		{
			BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.None, BuildTarget.iOS);
		}
	}


	[MenuItem("Asset Bundles/Build Win")]
	static void BuildBundlesWin()
	{
		string path = EditorUtility.SaveFolderPanel("Save WIN bundles to...", "", ""); // dialog window
		if (path.Length != 0)
		{
			BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
		}
	}


	[MenuItem("Asset Bundles/Get AssetBundle names")]
	static void GetNames()
	{
		var names = AssetDatabase.GetAllAssetBundleNames();
		foreach (var name in names)
		{
			Debug.Log("AssetBundle: " + name);
		}
	}

}
#endif