﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Buttons : MonoBehaviour
{

	public Sprite normalSprite, pressedSprite;

	void OnMouseDown()
	{
		GetComponent<SpriteRenderer>().sprite = pressedSprite;
	}

	void OnMouseUp()
	{
		GetComponent<SpriteRenderer>().sprite = normalSprite;
	}

	void OnMouseUpAsButton()
	{

		switch (gameObject.name)
		{
			case "PlayButton":
				SceneManager.LoadScene("GameScene");
				break;
		}
	}

}
